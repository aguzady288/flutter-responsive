import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:outlook/constants.dart';
import 'package:outlook/screens/main/bloc/messagesBloc.dart';
import 'package:outlook/screens/main/main_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<messagesBloc>(
          create: (BuildContext context) => messagesBloc([]),
        )
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Tes',
        theme: ThemeData(),
        home: MainScreen(),
      ),
    );
  }
}
