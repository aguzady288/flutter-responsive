import 'package:flutter/material.dart';
import 'package:outlook/responsive.dart';
import 'package:outlook/screens/main/main_screen.dart';
import 'package:outlook/screens/pasien/main_pasien.dart';
import 'package:websafe_svg/websafe_svg.dart';

import '../constants.dart';
import '../extensions.dart';
import 'side_menu_item.dart';
import 'tags.dart';

import 'package:flutter/foundation.dart' show kIsWeb;

class SideMenu extends StatefulWidget {
  SideMenu({
    Key key, this.menu
  }) : super(key: key);
  int menu = 1;

  @override
  _SideMenuState createState() => _SideMenuState();
}

class _SideMenuState extends State<SideMenu> {


  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      padding: EdgeInsets.only(top: kIsWeb ? kDefaultPadding : 0),
      color: kBgLightColor,
      child: SafeArea(
        child: SingleChildScrollView(
          padding: EdgeInsets.symmetric(horizontal: kDefaultPadding),
          child: Column(
            children: [
              Row(
                children: [
                  Image.asset(
                    "assets/images/logo.png",
                    height: 40,
                  ),
                  Spacer(),
                  // We don't want to show this close button on Desktop mood
                  if (!Responsive.isDesktop(context)) CloseButton(),
                ],
              ),
              SizedBox(height: kDefaultPadding),
              FlatButton.icon(
                minWidth: double.infinity,
                padding: EdgeInsets.symmetric(
                  vertical: kDefaultPadding,
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                color: (widget.menu == 1) ? kPrimaryColor : kBgDarkColor,
                onPressed: () {
                  widget.menu = 1;
                  setState(() {});
                  if(!Responsive.isDesktop(context)) {
                    Navigator.pop(context);
                  }
                },
                icon: Image.asset(
                  (widget.menu == 1)
                      ? "assets/images/severity_putih.png"
                      : "assets/images/severity.png",
                  width: 20,
                  height: 20,
                ),
                label: Text(
                  "Overview",
                  style:
                      TextStyle(color: (widget.menu == 1) ? Colors.white : kTextColor),
                ),
              ).addNeumorphism(
                topShadowColor: Colors.white,
                bottomShadowColor: Color(0xFF234395).withOpacity(0.2),
              ),
              SizedBox(height: kDefaultPadding),
              FlatButton.icon(
                minWidth: double.infinity,
                padding: EdgeInsets.symmetric(
                  vertical: kDefaultPadding,
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                color: (widget.menu == 2) ? kPrimaryColor : kBgDarkColor,
                onPressed: () {
                  widget.menu = 2;
                  setState(() {});
                  if(!Responsive.isDesktop(context)) {
                    Navigator.pop(context);
                  }
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (context) =>
                          main_pasien(),
                    ),
                  );
                },
                icon: Image.asset(
                  (widget.menu == 2)
                      ? "assets/images/people_putih.png"
                      : "assets/images/people.png",
                  width: 20,
                  height: 20,
                ),
                label: Text(
                  "Patient List",
                  style:
                      TextStyle(color: (widget.menu == 2) ? Colors.white : kTextColor),
                ),
              ).addNeumorphism(),
              SizedBox(height: kDefaultPadding),
              FlatButton.icon(
                minWidth: double.infinity,
                padding: EdgeInsets.symmetric(
                  vertical: kDefaultPadding,
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                color: (widget.menu == 3) ? kPrimaryColor : kBgDarkColor,
                onPressed: () {
                  widget.menu = 3;
                  if(!Responsive.isDesktop(context)) {
                    Navigator.pop(context);
                  } else {
                    setState(() {});
                  }
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (context) =>
                          MainScreen(),
                    ),
                  );
                },
                icon: Image.asset(
                  widget.menu == 3
                      ? "assets/images/chat_message_putih.png"
                      : "assets/images/chat_message.png",
                  width: 20,
                  height: 20,
                ),
                label: Text(
                  "Messages",
                  style:
                      TextStyle(color: (widget.menu == 3) ? Colors.white : kTextColor),
                ),
              ).addNeumorphism(),
              SizedBox(height: kDefaultPadding),
              FlatButton.icon(
                minWidth: double.infinity,
                padding: EdgeInsets.symmetric(
                  vertical: kDefaultPadding,
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                color: (widget.menu == 4) ? kPrimaryColor : kBgDarkColor,
                onPressed: () {
                  widget.menu = 4;
                  setState(() {});
                  if(!Responsive.isDesktop(context)) {
                    Navigator.pop(context);
                  }
                },
                icon: Image.asset(
                  widget.menu == 4
                      ? "assets/images/money_bag_putih.png"
                      : "assets/images/money_bag.png",
                  width: 20,
                  height: 20,
                ),
                label: Text(
                  "Payment",
                  style:
                      TextStyle(color: (widget.menu == 4) ? Colors.white : kTextColor),
                ),
              ).addNeumorphism(),
              SizedBox(height: kDefaultPadding),
              FlatButton.icon(
                minWidth: double.infinity,
                padding: EdgeInsets.symmetric(
                  vertical: kDefaultPadding,
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                color: (widget.menu == 5) ? kPrimaryColor : kBgDarkColor,
                onPressed: () {
                  widget.menu = 5;
                  setState(() {});
                  if(!Responsive.isDesktop(context)) {
                    Navigator.pop(context);
                  }
                },
                icon: Image.asset(
                  widget.menu == 5
                      ? "assets/images/settings_putih.png"
                      : "assets/images/settings.png",
                  width: 20,
                  height: 20,
                ),
                label: Text(
                  "Settings",
                  style:
                      TextStyle(color: (widget.menu == 5) ? Colors.white : kTextColor),
                ),
              ).addNeumorphism(),
            ],
          ),
        ),
      ),
    );
  }
}
