import 'package:flutter/material.dart';
import 'package:outlook/constants.dart';
import 'package:outlook/models/User_data.dart';

class detail_pasien extends StatefulWidget {
  detail_pasien({Key key, this.user_data}) : super(key: key);

  User_data user_data;

  @override
  _detail_pasienState createState() => _detail_pasienState();
}

class _detail_pasienState extends State<detail_pasien> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Detail Patient",
          style: TextStyle(color: kPrimaryColor),
        ),
        backgroundColor: Colors.white,
        elevation: 1,
        iconTheme: IconThemeData(color: kPrimaryColor),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: 20, right: 20, bottom: 20, top: 20),
            child: Card(
              elevation: 5,
              shadowColor: kGrayColor,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  SizedBox(
                    height: 16,
                    width: double.infinity,
                  ),
                  SizedBox(
                    width: 100,
                    height: 100,
                    child: CircleAvatar(
                      backgroundColor: Colors.transparent,
                      backgroundImage: AssetImage(widget.user_data.image),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Text(
                    widget.user_data.name,
                    style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: 16,
                        color: Colors.black87),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Text(
                    widget.user_data.subject,
                    style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 12,
                        color: Colors.black54),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Padding(
                    padding: EdgeInsets.all(kDefaultPadding),
                    child: Text(
                      emailDemoText,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 12,
                          color: Colors.black54),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
