import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:outlook/components/side_menu.dart';
import 'package:outlook/constants.dart';
import 'package:outlook/models/User_data.dart';
import 'package:outlook/responsive.dart';

import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:outlook/screens/pasien/ui/detail_pasien.dart';
import 'package:outlook/screens/pasien/ui/pasien_card.dart';

class list_pasien extends StatefulWidget {
  list_pasien({
    Key key, this.main_class
  }) : super(key: key);

  var main_class;

  @override
  _list_pasienState createState() => _list_pasienState();
}

class _list_pasienState extends State<list_pasien> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  TextEditingController searchController = TextEditingController();
  int index_selected;
  List search_billingList = [];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      drawer: ConstrainedBox(
        constraints: BoxConstraints(maxWidth: 250),
        child: SideMenu(menu: 2),
      ),
      body: Container(
        padding: EdgeInsets.only(top: kIsWeb ? kDefaultPadding : 0),
        color: kBgDarkColor,
        child: SafeArea(
          right: false,
          child: Column(
            children: [
              Padding(
                padding:
                const EdgeInsets.symmetric(horizontal: kDefaultPadding),
                child: Row(
                  children: [
                    if (!Responsive.isDesktop(context))
                      IconButton(
                        icon: Icon(Icons.menu),
                        onPressed: () {
                          _scaffoldKey.currentState.openDrawer();
                        },
                      ),
                    if (!Responsive.isDesktop(context)) SizedBox(width: 5),
                    Text("Data Patient", style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600, color: kPrimaryColor),)
                  ],
                ),
              ),
              SizedBox(height: 8),
              Padding(
                padding: EdgeInsets.only(left: 16, right: 16),
                child: Container(
                  height: 40,
                  child: TextField(
                    controller: searchController,
                    onChanged: (value) {
                      onSearchTextChanged(value);
                    },
                    decoration: InputDecoration(
                        hintText: "Search",
                        fillColor: Colors.white,
                        filled: true,
                        suffixIcon: Icon(Icons.search_outlined, color: kTextColor,),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                          borderSide: BorderSide.none,
                        ),
                        contentPadding: EdgeInsets.only(top: 8, left: 16)
                    ),
                  ),
                ),
              ),
              SizedBox(height: kDefaultPadding),
              Expanded(
                child: searchController.text.isEmpty ? ListView.builder(
                  itemCount: emails.length,
                  itemBuilder: (context, index) => pasien_card(
                    isActive: Responsive.isMobile(context) ? false : index == index_selected,
                    user: emails[index],
                    press: () {
                      emails[index].isChecked = true;
                      if (Responsive.isMobile(context)) {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) =>
                                detail_pasien(user_data: emails[index],),
                          ),
                        );
                      } else {
                        index_selected = index;
                        widget.main_class.pasien_terpilih = emails[index];
                        widget.main_class.setState(() {});
                      }
                    },
                  ),
                ) : ListView.builder(
                  itemCount: search_billingList.length,
                  itemBuilder: (context, index) => pasien_card(
                    isActive: Responsive.isMobile(context) ? false : index == index_selected,
                    user: search_billingList[index],
                    press: () {
                      search_billingList[index].isChecked = true;
                      if (Responsive.isMobile(context)) {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) =>
                                detail_pasien(user_data: search_billingList[index],),
                          ),
                        );
                      } else {
                        index_selected = index;
                        widget.main_class.pasien_terpilih = search_billingList[index];
                        widget.main_class.setState(() {});
                      }
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  onSearchTextChanged(String text) async {
    search_billingList.clear();
    index_selected = null;
    if (text.isEmpty) {
      setState(() {});
      return;
    }

    emails.forEach((data) {
      if (data.name.toLowerCase().contains(text.toLowerCase()) ||
          data.subject
              .toLowerCase()
              .contains(text.toLowerCase())) {
        search_billingList.add(data);
      }
    });
    setState(() {});
  }
}
