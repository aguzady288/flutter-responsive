import 'package:flutter/material.dart';
import 'package:outlook/components/side_menu.dart';
import 'package:outlook/models/User_data.dart';
import 'package:outlook/responsive.dart';
import 'package:outlook/screens/pasien/ui/detail_pasien.dart';
import 'package:outlook/screens/pasien/ui/list_pasien.dart';

class main_pasien extends StatefulWidget {
  const main_pasien({Key key}) : super(key: key);

  @override
  _main_pasienState createState() => _main_pasienState();
}

class _main_pasienState extends State<main_pasien> {
  User_data pasien_terpilih;

  @override
  Widget build(BuildContext context) {
    Size _size = MediaQuery.of(context).size;
    return Scaffold(
      body: Responsive(
        mobile: list_pasien(main_class: this,),
        tablet: Row(
          children: [
            Expanded(
              flex: 6,
              child: list_pasien(main_class: this,),
            ),
            Expanded(
              flex: 8,
              child: FutureBuilder(builder: (ctx, AsyncSnapshot snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return Center(
                    child: SizedBox(
                      width: 50,
                      height: 50,
                      child: CircularProgressIndicator(),
                    ),
                  );
                } else {
                  if(pasien_terpilih != null) {
                    return detail_pasien(user_data: pasien_terpilih,);
                  } else {
                    return Container();
                  }
                }
              }),
            ),
          ],
        ),
        desktop: Row(
          children: [
            Expanded(
              flex: _size.width > 1340 ? 2 : 3,
              child: SideMenu(menu: 2,),
            ),
            Expanded(
              flex: _size.width > 1340 ? 4 : 6,
              child: list_pasien(main_class: this,),
            ),
            Expanded(
              flex: _size.width > 1340 ? 8 : 8,
              child: FutureBuilder(builder: (ctx, AsyncSnapshot snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return Center(
                    child: SizedBox(
                      width: 50,
                      height: 50,
                      child: CircularProgressIndicator(),
                    ),
                  );
                } else {
                  if(pasien_terpilih != null) {
                    return detail_pasien(user_data: pasien_terpilih,);
                  } else {
                    return Container();
                  }
                }
              }),
            ),
          ],
        ),
      ),
    );
  }
}
