import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:outlook/components/side_menu.dart';
import 'package:outlook/models/User_data.dart';
import 'package:outlook/responsive.dart';
import 'package:outlook/screens/email/email_screen.dart';
import 'package:outlook/screens/main/components/view_chat_message.dart';
import 'package:websafe_svg/websafe_svg.dart';
import 'package:flutter_chat_types/flutter_chat_types.dart' as types;

import '../../../constants.dart';
import 'list_chat_card.dart';

import 'package:flutter/foundation.dart' show kIsWeb;

class list_chat_message extends StatefulWidget {
  list_chat_message({
    Key key, this.main_class
  }) : super(key: key);

  var main_class;

  @override
  _list_chat_messageState createState() => _list_chat_messageState();
}

class _list_chat_messageState extends State<list_chat_message> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  int index_selected;
  List search_billingList = [];
  TextEditingController searchController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      drawer: ConstrainedBox(
        constraints: BoxConstraints(maxWidth: 250),
        child: SideMenu(menu: 3),
      ),
      body: Container(
        padding: EdgeInsets.only(top: kIsWeb ? kDefaultPadding : 0),
        color: kBgDarkColor,
        child: SafeArea(
          right: false,
          child: Column(
            children: [
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: kDefaultPadding),
                child: Row(
                  children: [
                    if (!Responsive.isDesktop(context))
                      IconButton(
                        icon: Icon(Icons.menu),
                        onPressed: () {
                          _scaffoldKey.currentState.openDrawer();
                        },
                      ),
                    if (!Responsive.isDesktop(context)) SizedBox(width: 5),
                    Expanded(
                      child: Container(
                        height: 40,
                        child: TextField(
                          controller: searchController,
                          onChanged: (value) {
                            onSearchTextChanged(value);
                          },
                          decoration: InputDecoration(
                            hintText: "Search",
                            fillColor: Colors.white,
                            filled: true,
                            suffixIcon: Icon(Icons.search_outlined, color: kTextColor,),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.all(Radius.circular(20)),
                              borderSide: BorderSide.none,
                            ),
                            contentPadding: EdgeInsets.only(top: 8, left: 16)
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: kDefaultPadding),
              if(emails != null)
              Expanded(
                child: (searchController.text.isEmpty) ? ListView.builder(
                  itemCount: emails.length,
                  itemBuilder: (context, index) => list_chat_card(
                    isActive: Responsive.isMobile(context) ? false : index == index_selected,
                    user: emails[index],
                    press: () {
                      emails[index].isChecked = true;
                      if (Responsive.isMobile(context)) {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) =>
                                view_chat_message(
                                  list_chat: this, user_data: emails[index],),
                          ),
                        );
                      } else {
                        index_selected = index;
                        widget.main_class.user_data = emails[index];
                        widget.main_class.obj_list_chat = this;
                        widget.main_class.setState(() {});
                      }
                    },
                  ),
                ) : ListView.builder(
                  itemCount: search_billingList.length,
                  itemBuilder: (context, index) => list_chat_card(
                    isActive: Responsive.isMobile(context) ? false : index == index_selected,
                    user: search_billingList[index],
                    press: () {
                      search_billingList[index].isChecked = true;
                      if (Responsive.isMobile(context)) {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) =>
                                view_chat_message(
                                  list_chat: this, user_data: search_billingList[index],),
                          ),
                        );
                      } else {
                        index_selected = index;
                        widget.main_class.user_data = search_billingList[index];
                        widget.main_class.obj_list_chat = this;
                        widget.main_class.setState(() {});
                      }
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  onSearchTextChanged(String text) async {
    search_billingList.clear();
    index_selected = null;
    if (text.isEmpty) {
      setState(() {});
      return;
    }

    emails.forEach((data) {
      if (data.name.toLowerCase().contains(text.toLowerCase())) {
        search_billingList.add(data);
      }
    });
    setState(() {});
  }
}
