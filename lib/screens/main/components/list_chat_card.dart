import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:outlook/models/User_data.dart';

import '../../../constants.dart';
import '../../../extensions.dart';

class list_chat_card extends StatelessWidget {
  const list_chat_card({
    Key key,
    this.isActive = true,
    this.user,
    this.press,
  }) : super(key: key);

  final bool isActive;
  final User_data user;
  final VoidCallback press;

  @override
  Widget build(BuildContext context) {
    List data_chat = jsonDecode(user.body);
    return Padding(
      padding: EdgeInsets.symmetric(
          horizontal: kDefaultPadding, vertical: 4),
      child: InkWell(
        onTap: press,
        child: Stack(
          children: [
            Container(
              padding: EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10),
              decoration: BoxDecoration(
                color: isActive ? kPrimaryColor : kSecondaryColor,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Column(
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(
                        width: 32,
                        child: CircleAvatar(
                          backgroundColor: Colors.transparent,
                          backgroundImage: AssetImage(user.image),
                        ),
                      ),
                      SizedBox(width: kDefaultPadding / 2),
                      Expanded(
                        child: Text.rich(
                          TextSpan(
                            text: "${user.name} \n",
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w700,
                              color: isActive ? Colors.white : kTextColor,
                            ),
                            children: [
                              TextSpan(
                                text: data_chat[0]['text'].toString() ?? "",
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText2
                                    .copyWith(
                                      color:
                                          isActive ? Colors.white : kTextColor,
                                    ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Text(
                        user.time,
                        style: Theme.of(context).textTheme.caption.copyWith(
                          color: isActive ? Colors.white70 : null,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ).addNeumorphism(
              blurRadius: 15,
              borderRadius: 15,
              offset: Offset(5, 5),
              topShadowColor: Colors.white60,
              bottomShadowColor: Color(0xFF234395).withOpacity(0.15),
            ),
            if (!user.isChecked)
              Positioned(
                right: 20,
                top: 28,
                child: Container(
                  height: 20,
                  width: 20,
                  child: Center(child: Text(user.unread.toString(), style: TextStyle(fontSize: 10, fontWeight: FontWeight.w500, color: Colors.white),)),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: kPrimaryColor,
                  ),
                ).addNeumorphism(
                  blurRadius: 4,
                  borderRadius: 8,
                  offset: Offset(2, 2),
                ),
              ),
          ],
        ),
      ),
    );
  }
}
