import 'package:flutter/material.dart';
import 'package:outlook/components/side_menu.dart';
import 'package:outlook/models/User_data.dart';
import 'package:outlook/responsive.dart';
import 'package:outlook/screens/email/email_screen.dart';
import 'package:outlook/screens/main/components/view_chat_message.dart';
import 'components/list_chat_message.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  User_data user_data;
  var obj_list_chat;

  @override
  Widget build(BuildContext context) {
    Size _size = MediaQuery.of(context).size;
    list_chat_message list_chat = list_chat_message(main_class : this);

    return Scaffold(
      body: Responsive(
        mobile: list_chat,
        tablet: Row(
          children: [
            Expanded(
              flex: 6,
              child: list_chat,
            ),
            Expanded(
              flex: 8,
              child: FutureBuilder(
                  builder: (ctx, AsyncSnapshot snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return Center(child: SizedBox(width: 50,height: 50,child: CircularProgressIndicator(),),);
                    } else {
                      if (user_data != null) {
                        return view_chat_message(list_chat: obj_list_chat, user_data: user_data,);
                      } else {
                        return Container();
                      }
                    }
                  }
              ),
            ),
          ],
        ),
        desktop: Row(
          children: [
            Expanded(
              flex: _size.width > 1340 ? 2 : 3,
              child: SideMenu(menu: 3,),
            ),
            Expanded(
              flex: _size.width > 1340 ? 4 : 6,
              child: list_chat,
            ),
            Expanded(
              flex: _size.width > 1340 ? 8 : 8,
              child: FutureBuilder(
                  builder: (ctx, AsyncSnapshot snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return Center(child: SizedBox(width: 50,height: 50,child: CircularProgressIndicator(),),);
                    } else {
                      if (user_data != null) {
                        return view_chat_message(list_chat: obj_list_chat, user_data: user_data,);
                      } else {
                        return Container();
                      }
                    }
                  }
              ),
            ),
          ],
        ),
      ),
    );
  }
}
