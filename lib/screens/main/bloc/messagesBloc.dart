import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_chat_types/flutter_chat_types.dart' as types;

class messagesBloc extends Bloc<List<types.Message>, List<types.Message>> {
  messagesBloc(List<types.Message> value) : super([]);

  @override
  Stream<List<types.Message>> mapEventToState(
      List<types.Message> event) async* {
    yield event;
  }
}
