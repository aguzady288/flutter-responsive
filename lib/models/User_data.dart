import 'package:flutter/material.dart';

class User_data {
  String image, name, subject, time;
  bool isAttachmentAvailable, isChecked;
  final Color tagColor;
  final int unread;
  String body;

  User_data({
    this.time,
    this.isChecked,
    this.image,
    this.name,
    this.subject,
    this.body,
    this.isAttachmentAvailable,
    this.tagColor,
    this.unread,
  });

}

List<User_data> emails = List.generate(
  demo_data.length,
  (index) => User_data(
    name: demo_data[index]['name'],
    image: demo_data[index]['image'],
    subject: demo_data[index]['subject'],
    isAttachmentAvailable: demo_data[index]['isAttachmentAvailable'],
    isChecked: demo_data[index]['isChecked'],
    tagColor: demo_data[index]['tagColor'],
    time: demo_data[index]['time'],
    body: demo_data[index]['body'],
    unread: demo_data[index]['unread'],
  ),
);

List demo_data = [
  {
    "name": "Apple",
    "image": "assets/images/user_1.png",
    "subject": "087846372847",
    "isAttachmentAvailable": false,
    "isChecked": true,
    "tagColor": null,
    "time": "Now",
    "body" : "[{\"author\":{\"firstName\":\"Alex\",\"id\":\"b4878b96-efbc-479a-8291-474ef323dec7\",\"imageUrl\":\"https://avatars.githubusercontent.com/u/14123304?v=4\"},\"createdAt\":1598438797000,\"id\":\"e7a673e9-86eb-4572-936f-2882b0183cdc\",\"status\":\"seen\",\"text\":\"Hello ?\",\"type\":\"text\"},{\"author\":{\"firstName\":\"Alex\",\"id\":\"b4878b96-efbc-479a-8291-474ef323dec7\",\"imageUrl\":\"https://avatars.githubusercontent.com/u/14123304?v=4\"},\"createdAt\":1598438784000,\"id\":\"8fa70836-3309-4d09-a777-4d9603e1f125\",\"mimeType\":\"image/jpg\",\"name\":\"image.jpg\",\"size\":59645,\"status\":\"seen\",\"type\":\"file\",\"uri\":\"https://hcti.io/v1/image/c1ea794e-32c7-4516-bd0f-8b835e8a2f2e\"}]",
    "unread" : 0
  },
  {
    "name": "Elvia Atkins",
    "image": "assets/images/user_2.png",
    "subject": "0838734938584",
    "isAttachmentAvailable": true,
    "isChecked": false,
    "tagColor": null,
    "time": "15:32",
    "body" : "[{\"author\":{\"firstName\":\"Alex\",\"id\":\"b4878b96-efbc-479a-8291-474ef323dec7\",\"imageUrl\":\"https://avatars.githubusercontent.com/u/14123304?v=4\"},\"createdAt\":1598438797000,\"id\":\"e7a673e9-86eb-4572-936f-2882b0183cdc\",\"status\":\"seen\",\"text\":\"Good Afternoon Doc ?\",\"type\":\"text\"}]",
    "unread" : 1
  },
  {
    "name": "Marvin Kiehn",
    "image": "assets/images/user_3.png",
    "subject": "081384739274",
    "isAttachmentAvailable": true,
    "isChecked": false,
    "tagColor": null,
    "time": "14:27",
    "body" : "[{\"author\":{\"firstName\":\"Alex\",\"id\":\"b4878b96-efbc-479a-8291-474ef323dec7\",\"imageUrl\":\"https://avatars.githubusercontent.com/u/14123304?v=4\"},\"createdAt\":1598438797000,\"id\":\"e7a673e9-86eb-4572-936f-2882b0183cdc\",\"status\":\"seen\",\"text\":\"Selamat Sore\",\"type\":\"text\"}]",
    "unread" : 1
  },
  {
    "name": "Domenic Bosco",
    "image": "assets/images/user_4.png",
    "subject": "085274836473",
    "isAttachmentAvailable": false,
    "isChecked": true,
    "tagColor": Color(0xFF23CF91),
    "time": "17:46",
    "body" : "[{\"author\":{\"firstName\":\"Alex\",\"id\":\"b4878b96-efbc-479a-8291-474ef323dec7\",\"imageUrl\":\"https://avatars.githubusercontent.com/u/14123304?v=4\"},\"createdAt\":1598438797000,\"id\":\"e7a673e9-86eb-4572-936f-2882b0183cdc\",\"status\":\"seen\",\"text\":\"halo\",\"type\":\"text\"}]",
    "unread" : 0
  },
  {
    "name": "Elenor Bauch",
    "image": "assets/images/user_5.png",
    "subject": "0858923738272",
    "isAttachmentAvailable": false,
    "isChecked": false,
    "tagColor": Color(0xFF3A6FF7),
    "time": "9:58",
    "body" : "[{\"author\":{\"firstName\":\"Alex\",\"id\":\"b4878b96-efbc-479a-8291-474ef323dec7\",\"imageUrl\":\"https://avatars.githubusercontent.com/u/14123304?v=4\"},\"createdAt\":1598438797000,\"id\":\"e7a673e9-86eb-4572-936f-2882b0183cdc\",\"status\":\"seen\",\"text\":\"Terima kasih\",\"type\":\"text\"},{\"author\":{\"firstName\":\"Alex\",\"id\":\"b4878b96-efbc-479a-8291-474ef323dec7\",\"imageUrl\":\"https://avatars.githubusercontent.com/u/14123304?v=4\"},\"createdAt\":1598438797000,\"id\":\"e7a673e9-86eb-4572-936f-2882b0183cdc\",\"status\":\"seen\",\"text\":\"Pelayanannya sangat baik\",\"type\":\"text\"}]",
    "unread" : 2
  }
];

String emailDemoText =
    "Corporis illo provident. Sunt omnis neque et aperiam. Nemo ut dolorum fugit eum sed. Corporis illo provident. Sunt omnis neque et aperiam. Nemo ut dolorum fugit eum sed. Corporis illo provident. Sunt omnis neque et aperiam. Nemo ut dolorum fugit eum sed";
